#include "dex.h"

#ifndef PAD_H
#define PAD_H

#define DEXFILE (char*)"assets://dex/pad.dex"
#define CLASSNAME (char*)"com.xl.dex.xl_keyPad"

extern _DEX*padDex;

extern void initPad(char *data);

#endif

