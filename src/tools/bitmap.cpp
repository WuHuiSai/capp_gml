
#include "bitmap.h"

#ifdef __cplusplus
extern "C"{ //因为cpp文件默认定义了该宏),则采用C语言方式进行编译
#endif

//获取bitmap信息
void bitmap_getInfo(int bitmap, BITMAPINFO *info)
{
 bitmapGetInfo(bitmap,info);
 
}

//获取bitmap上一个像素的颜色 abgr格式
int bitmap_getPix(BITMAPINFO *info, int x,int y)
{
 int *ptr=(int*)info->ptr;
 return *(ptr + info->width*y+x);
}


//设置bitmap上一个像素的颜色 abgr格式
int bitmap_setPix(BITMAPINFO *info, int x,int y,int color)
{
 int *ptr=(int*)info->ptr;
 *(ptr + info->width*y+x)=color;
 return 0;
}

//获取bitmap宽度
int bitmap_getWidth(BITMAPINFO *info)
{
 return info->width;
}

//获取bitmap高度
int bitmap_getHeight(BITMAPINFO *info)
{
 return info->height;
}

//将bitmap渲染成指定颜色，返回渲染后的bitmap
int bitmap_drawColor(int bitmap,int color)
{
 int r=(color>>16) & 0xff;
 int g=(color>>8) & 0xff;
 int b=color & 0xff;
 int br,bg,bb,alpha;
 color = color<<8;
 BITMAPINFO *info= (BITMAPINFO*)
 malloc(sizeof(BITMAPINFO));
 BITMAPINFO *temp_info= (BITMAPINFO*)
 malloc(sizeof(BITMAPINFO));
 bitmapGetInfo(bitmap, info);
 int temp_bitmap=
 createBitmap(info->width,
info->height);
 bitmapGetInfo(temp_bitmap, temp_info);
 int ix,iy;
 for(iy=0;iy<info->height;iy++)
 {
  for(ix=0;ix<info->width;ix++)
  {
   int pix = bitmap_getPix(info,ix,iy);
   br=(pix) & 0xff;
   bg=(pix>>8) & 0xff;
   bb=(pix>>16) & 0xff;
   alpha=(pix>>24) & 0xff;
   //获取平均值
   int ap= (br+bg+bb)/3;
   //合成渲染色
   br= r * ap/0xff;
   bg= g * ap/0xff;
   bb= b * ap/0xff;
   //生成
   bitmap_setPix(temp_info,ix,iy,MAKEABGR(alpha,bb,bg,br));
  }
 }
 free(info);
 free(temp_info);
 return temp_bitmap;
}


//将bitmap渲染成白色，返回渲染后的bitmap
int bitmap_white(int bitmap,int light)
{
 int br,bg,bb,alpha;
 
 BITMAPINFO *info= (BITMAPINFO*)
 malloc(sizeof(BITMAPINFO));
 BITMAPINFO *temp_info= (BITMAPINFO*)
 malloc(sizeof(BITMAPINFO));
 bitmapGetInfo(bitmap, info);
 int temp_bitmap= createBitmap(info->width,info->height);
 bitmapGetInfo(temp_bitmap, temp_info);
 int ix,iy;
 for(iy=0;iy<info->height;iy++)
 {
  for(ix=0;ix<info->width;ix++)
  {
   int pix = bitmap_getPix(info,ix,iy);
   br=(pix) & 0xff;
   bg=(pix>>8) & 0xff;
   bb=(pix>>16) & 0xff;
   alpha=(pix>>24) & 0xff;
   //获取平均值
   int ap= (br+bg+bb)/3;
   
   br = br+(0xff-br)*light/0xff;
   bg = bg+(0xff-bg)*light/0xff;
   bb = bb+(0xff-bb)*light/0xff;
   //生成
   bitmap_setPix(temp_info,ix,iy,MAKEABGR(alpha,bb,bg,br));
  }
 }
 free(info);
 free(temp_info);
 return temp_bitmap;
}


#ifdef __cplusplus
}
#endif



