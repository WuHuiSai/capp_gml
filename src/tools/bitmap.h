#ifndef _BITMAP_H_
#define _BITMAP_H_

#include "base.h"
#include "graphics.h"

#define MAKERGBA(r,g,b,a) ((r<<24) | (g<<16) | (b<<8) | a)

#define MAKEABGR(a,b,g,r) ((a<<24) | (b<<16) | (g<<8) | r)



#define MAKEARGB(a,r,g,b) ((a<<24) | (r<<16) | (g<<8) | b)

#define MAKECOLOR(r,g,b) ((r<<16) | (g<<8) | b)

#ifdef __cplusplus
extern "C"{ //因为cpp文件默认定义了该宏),则采用C语言方式进行编译
#endif


//获取bitmap信息
extern void bitmap_getInfo(int bitmap, BITMAPINFO *info);


//获取bitmap上一个像素的颜色 abgr格式
extern int bitmap_getPix(BITMAPINFO *info, int x,int y);



//设置bitmap上一个像素的颜色 abgr格式
extern int bitmap_setPix(BITMAPINFO *info, int x,int y,int color);

//获取bitmap宽度
extern int bitmap_getWidth(BITMAPINFO *info);


//获取bitmap高度
extern int bitmap_getHeight(BITMAPINFO *info);


//将bitmap渲染成指定颜色，返回渲染后的 bitmap
extern int bitmap_drawColor(int bitmap,int color);



//将bitmap渲染成白色，返回渲染后的bitmap
extern int bitmap_white(int bitmap,int light);



#ifdef __cplusplus
}
#endif



#endif

