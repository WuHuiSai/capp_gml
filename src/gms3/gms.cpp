//加载系统头文件base.h
#include "base.h"
#include "exb.h"
#include "ex_math.h"
#include "graphics.h"
#include "android.h"

#include "pad.h"

#include "gml.h"
#include "sprite.h"
#include "object.h"
#include "room.h"

#ifdef __cplusplus
extern "C"{
#endif

using namespace gml;
extern int main();

//入口函数
int init()
{
 //记录真实宽高
 screen_width = SCRW;
 screen_height = SCRH;
 
 game_fps = fps_create();
 
 //调用入口函数
 main();
 return 0;
}

//event函数
int event(int type,int p1,int p2)
{
 //计算坐标
 int x = p1+room::_room->view_pos_x;
 int y = p2+room::_room->view_pos_y;
 
 //循环object事件
 if(MS_DOWN==type||MS_UP==type
   ||MS_MOVE==type)
 {
  for(object*obj:object::all_object)
  {
   int next = 
   obj->event_mouse(type,x,y);
   if(!next)
   	break;
  }
 }
 else if(KY_DOWN==type||KY_UP==type)
 {
  for(object*obj:object::all_object)
  {
   int next = 
   obj->event_key(type,p1);
   if(!next)
   	break;
  }
 }
 else
 {
  for(object*obj:object::all_object)
  {
   int next = 
   obj->event_event(type,p1,p2);
   if(!next)
   	break;
  }
 }
 return 0;
}

//应用暂停
int pause()
{
 for(object*obj:object::all_object)
 {
  int next = 
  obj->event_pause();
  if(!next)
  	break;
 }
 return 0;
}

//应用恢复
int resume()
{
 for(object*obj:object::all_object)
 {
  int next = 
  obj->event_resume();
  if(!next)
  	break;
 }
 return 0;
}

//应用退出函数
int exitApp()
{
 if(padDex)
 	dex_free(padDex);
 
 //释放fps工具
 fps_free(game_fps);
 
 //释放room
 for(room*rm:room::all_room)
 	delete rm;
 
 //释放object
 for(object*obj:object::all_object)
 	delete obj;
 
 //释放sprite
 for(sprite*spr:sprite::all_sprite)
 	delete spr;
 return 0;
}

#ifdef __cplusplus
}
#endif
