
#include "sprite.h"

namespace gml
{
 using std::string;
 using std::vector;
 
 //初始化静态变量
 std::list<sprite*> 
 sprite::all_sprite;

 //创建sprite
 sprite::sprite(string path)
 {
  //中心
  offset_x = 0;
  offset_y = 0;
  
  //宽高
  width = 0;
  height = 0;
  
  //载入
  this->loadSprite(path);
  
  //碰撞盒，相对于0,0
  bbox_top = 0;
  bbox_bottom = height;
  bbox_left = 0;
  bbox_right = width;
  
  //添加
  all_sprite.push_back(this); 
  printf("new sprite(%d,%d)\n",
  width,height);
 }
 
 //创建sprite
 sprite::sprite(string path,int w,int h):sprite(path)
 {
  offset_x = w;
  offset_y = h;
 }
 
 //释放sprite
 sprite::~sprite()
 {
  for(int32 image:sprite_image)
  {
   bitmapFree(image);
  }
  all_sprite.remove(this);
  printf("~sprite(%d)\n",this);
 }
 
 //创建sprite
 void sprite::loadSprite(string path)
 {
  this->path = path;
  int i = 0;
  while(TRUE)
  {
   string file = path+"/"+
   std::to_string(i)+".png";
   int32 bit = 
   readBitmapFromAssets(
    (char*)file.data());
   if(bit>0)
   {
    BITMAPINFO info;
    bitmapGetInfo(bit,&info);
    this->width = info.width;
    this->height = info.height;
    sprite_image.push_back(bit);
   }
   else
   	break;
   i++;
  }
  this->number = i;
 }
 
}