
#include "gml.h"


namespace gml
{
 //记录屏幕真实宽高
 int screen_width;
 int screen_height;
 
 FPS *game_fps;
 
 //碰撞判定
 //获取xy处的object
 object*position_meeting(int x,int y,object* ob)
 {
  object*re = NULL;
  for(object*obj:object::all_object)
  {
   if(obj->sprite_index==NULL)
    continue;
    
   //类型判断
   if(typeid(ob)==typeid(obj))
   if(collision_point(x,y,obj,0,0))
   	 re = obj;
  }
  return re;
 }
 
 //点于object
 int collision_point(int x,int y,object*obj,int prec,int notme)
 {
  int bx = obj->x
  -obj->sprite_index->offset_x
  +obj->sprite_index->bbox_left;
  int by = obj->y
  -obj->sprite_index->offset_y
  +obj->sprite_index->bbox_top;
  int bw = 
  obj->sprite_index->bbox_right
  -obj->sprite_index->bbox_left;
  int bh = 
  obj->sprite_index->bbox_bottom
  -obj->sprite_index->bbox_top;
  
  return isPointCollRect(x,y,
   bx,by,bw,bh);
 }
 
 //圆于object
 int collision_circle(int x1,int y1,int rad,object*obj,int prec,int notme)
 {
  int bx = obj->x
  -obj->sprite_index->offset_x
  +obj->sprite_index->bbox_left;
  int by = obj->y-
  -obj->sprite_index->offset_y
  +obj->sprite_index->bbox_top;
  int bw = 
  obj->sprite_index->bbox_right
  -obj->sprite_index->bbox_left;
  int bh = 
  obj->sprite_index->bbox_bottom
  -obj->sprite_index->bbox_top;
  
  return isCirCollRect(x1,y1,rad,
   bx,by,bw,bh);
 }
 
 //矩形于object
 int collision_rectangle(int x1,int y1,int x2,int y2,object*obj,int prec,int notme)
 {
  int bx = obj->x
  -obj->sprite_index->offset_x
  +obj->sprite_index->bbox_left;
  int by = obj->y
  -obj->sprite_index->offset_y
  +obj->sprite_index->bbox_top;
  int bw = 
  obj->sprite_index->bbox_right
  -obj->sprite_index->bbox_left;
  int bh = 
  obj->sprite_index->bbox_bottom
  -obj->sprite_index->bbox_top;
  
  return isCollRect(x1,y1,
  x2-x1,y2-y1,bx,by,bw,bh);
 }
 
 //房间函数
 void room_goto(room*room)
 {
  
  //清除上一个room
  if(room::_room&&room::_room!=room)
  {
   timerstop(room::_room->timer);
   delete room::_room;
  }
  
  //调用create事件
  room::_room = room;
  room->event_create();
 }
 
 //精灵函数
 void draw_sprite(sprite*spr,int img,int x,int y)
 {
  drawBitmap(spr->getBitmap(img),
   x-spr->offset_x,y-spr->offset_y);
 }
 
 //超级绘图
 void draw_sprite_ext(sprite*spr,int img,int x,int y,float xc,float yc,int rot,int color,int alpha)
 {
  int32 bit = 
  bitmap_drawColor(
  spr->getBitmap(img),color);
  drawBitmapExc(bit,x,y,
   (int)(spr->width*xc),
   (int)(spr->height*yc),
   0,0,spr->width,spr->height,
   spr->offset_x,spr->offset_y,
   rot,alpha);
  bitmapFree(bit);
 }
}