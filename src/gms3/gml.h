#ifndef GML_GML_H
#define GML_GML_H

#include "base.h"
#include "ex_math.h"

#include "object.h"
#include "sprite.h"
#include "room.h"

#include "../tools/bitmap.h"
#include "../tools/fps.h"

namespace gml
{
 extern int screen_width;
 extern int screen_height;
 extern FPS *game_fps;
 
 //碰撞函数
 //点xy处的object
 extern object*position_meeting(int x,int y,object*ob);
 
 //点xy与obj是否碰撞
 extern int collision_point(int x,int y,object*obj,int prec,int notme);
 
 //圆xy与obj是否碰撞
 extern int collision_circle(int x1,int y1,int rad,object*obj,int prec,int notme);
 
 //矩形xy与obj是否碰撞
 extern int collision_rectangle(int x1,int y1,int x2,int y2,object*obj,int prec,int notme);
 
 //房间函数
 extern void room_goto(room*room);
 
 //精灵函数
 extern void draw_sprite(sprite*spr,int img,int x,int y);
 
 extern void draw_sprite_ext(sprite*spr,int img,int x,int y,float xc,float yc,int rot,int color,int alpha);
}

#endif
