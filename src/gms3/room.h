#ifndef GML_ROOM_H
#define GML_ROOM_H

#include "base.h"
#include "object.h"

#include <list>

namespace gml
{
 //房间类
 class room
 {
  public:
  int32 timer;//定时器
  
  //宽高速度
  int width;
  int height;
  int room_speed;
  int background;
  
  //视野坐标
  int view_pos_x;
  int view_pos_y;
  
  //存储room
  static std::list<room*> 
  all_room;
  //当前房间
  static room* _room;
  
  room();
  //构造函数
  virtual ~room();
  //析构函数
  
  //事件定义
  virtual void event_create();
  //创建事件
  virtual void event_step();
 };
}

#endif