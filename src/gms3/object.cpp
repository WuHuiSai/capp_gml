#include "base.h"
#include "object.h"


namespace gml
{
 //初始化静态变量
 std::list<object*>
 object::all_object;
 //调试模式
 int object::debug = FALSE;
 
 object::object(int x,int y)
 {
  this->x = x;
  this->y = y;
  
  //默认不是固体
  this->solid = FALSE;
  
  //速度与属性
  speed = 0;
  direction = 0;
  
  //深度
  depth = 0;
  
  image_index = 0;
  image_speed = 0.5;
  
  image_alpha = 255;
  image_angle = 0;
  image_blend = 0;
  image_xscale = 1;
  image_yscale = 1;
  
  //可见
  visiable = TRUE;
  keep = FALSE;
  
  //定时器
  for(int i = 0;i<10;i++)
  	alarm[i] = 0;
  
  //添加
  all_object.push_back(this);
  
  sprite_index = NULL;
  printf("new object(%d)\n",this);
 }
 
 //构造函数
 object::object(int x,int y,sprite&spr):object(x,y)
 {
  sprite_index = &spr;
 }
 
 //析构函数
 object::~object()
 {
  all_object.remove(this);
  printf("~object(%d)\n",this);
 }
 
 //创建事件
 void object::event_create()
 {
  printf("obj%d create()\n",this);
 }
 
 //步事件
 void object::event_step()
 {
  //printf("obj%d step()\n",this);
 }
 
 //绘制事件
 void object::event_draw()
 {
  //printf("obj%d draw()\n",this);
  if(sprite_index==NULL)
   return;
   
  draw_sprite_ext(sprite_index,
   image_index,x,y,
   image_xscale,image_yscale,
   image_angle,image_blend,
   image_alpha);
  
  if(debug)
  {
   int bx = x-sprite_index->offset_x
   +sprite_index->bbox_left;
   int by = y-sprite_index->offset_y
   +sprite_index->bbox_top;
   int bw = sprite_index->bbox_right
   -sprite_index->bbox_left;
   int bh = sprite_index->bbox_bottom
   -sprite_index->bbox_top;
   drawRect(bx,by,bw,bh,0x80ff0000);
  }
 }
 
 //触屏事件
 int object::event_mouse(int type,int x,int y)
 {
  //printf("object event(%d)\n",type);
  return TRUE;
 }
 
 //键盘事件
 int object::event_key(int type,int key)
 {
  //printf("object event(%d)\n",type);
  return TRUE;
 }
 
 //除去触屏和按键
 int object::event_event(int type,int p1,int p2)
 {
  //printf("object event(%d)\n",type);
  return TRUE;
 }
 
 //定时器事件
 void object::event_alarm(int index)
 {
  printf("object alarm(%d)\n",index);
 }
 
 //后台事件
 int object::event_pause()
 {
  return TRUE;
 }
 
 //恢复事件
 int object::event_resume()
 {
  return TRUE;
 }
 
 //当前obj在xy碰撞的object
 object*object::place_meeting(int px,int py)
 {
  int bx = px-sprite_index->offset_x
  +sprite_index->bbox_left;
  int by = py-sprite_index->offset_y
  +sprite_index->bbox_top;
  int bw = sprite_index->bbox_right
  -sprite_index->bbox_left;
  int bh = sprite_index->bbox_bottom
  -sprite_index->bbox_top;
  
  object*re = NULL;
  for(object*obj:object::all_object)
  {
   if(obj->sprite_index==NULL)
    continue;
    
   if(obj!=this)
   if(obj->solid)
   	if(collision_rectangle(bx,by,
      bx+bw,by+bh,obj,0,0))
   	re = obj;
  }
  return re;
 }
}


