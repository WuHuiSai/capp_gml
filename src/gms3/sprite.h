#ifndef GML_SPRITE_H
#define GML_SPRITE_H

#include "base.h"
#include "graphics.h"
#include <string>
#include <vector>
#include <list>

namespace gml
{
 using std::string;
 using std::vector;

 class sprite
 {
  public:
  //路径
  string path;
  
  //中心坐标
  int offset_x;
  int offset_y;
  
  //宽高
  int width;
  int height;
  
  //碰撞盒，相对左上角
  int bbox_top;
  int bbox_bottom;
  int bbox_left;
  int bbox_right;
  
  //数量
  int number;
 
  //bitmap数组
  vector<int32> sprite_image;
  
  //存储sprite
  static std::list<sprite*> 
  all_sprite;
  
  //生成sprite
  sprite(string path);
  sprite(string path,int w,int h);
  ~sprite();
  
  //创建sprite
  void loadSprite(string path);
  
  //设置碰撞盒
  void setBbox(int top,int bom,int left,int right)
  {
   bbox_top = top;
   bbox_bottom = bom;
   bbox_left = left;
   bbox_right = right;
  }
  
  //获取当前bitmap
  int32 getBitmap(int index)
  {
   return sprite_image.at(
   index%sprite_image.size());
  }
 };
}

#endif