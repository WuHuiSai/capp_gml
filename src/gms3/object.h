#ifndef GML_OBJECT_H
#define GML_OBJECT_H

#include "base.h"
#include "ex_math.h"

//#include "gml.h"
#include "sprite.h"

#include <list>

namespace gml
{
 //精灵函数
 extern void draw_sprite(sprite*spr,int img,int x,int y);
 
 extern void draw_sprite_ext(sprite*spr,int img,int x,int y,float xc,float yc,int rot,int color,int alpha);
 
 //object类
 class object
 {
  public:
  int x;//坐标
  int y;
  
  int solid;//固体
  
  float speed;
  float direction;
  
  //深度
  int depth;
  
  //精灵
  sprite*sprite_index;
  float image_index;
  float image_speed;
  
  //图片属性
  int image_alpha;
  int image_angle;
  int image_blend;
  float image_xscale;
  float image_yscale;
  
  //是否可见
  int visiable;
  
  //保持到游戏结束
  int keep;
  
  //定时器
  float alarm[10];
  
  //存储object
  static std::list<object*>
  all_object;
  static int debug;
  
  object(int x,int y);
  object(int x,int y,sprite&spr);
  //构造函数
  virtual~object();
  //析构函数
  
  //事件定义
  virtual void event_create();
  //创建事件
  virtual void event_step();
  //步事件
  virtual void event_draw();
  //绘制事件
  virtual int event_event(int type,int p1,int p2);
  //除去按键和触屏
  virtual int event_key(int type,int key);
  //按键事件
  virtual int event_mouse(int type,int x,int y);
  //触屏事件
  virtual void event_alarm(int index);
  //定时器事件
  
  virtual int event_pause();
  //后台事件
  virtual int event_resume();
  //恢复事件
  
  //当前obj在xy碰撞的object
  virtual object*place_meeting(int px,int py);
 };
 
 //排序类
 class obj_depth_sort
 {
  public:
  bool operator()(const object*obj1,const object*obj2)
  {
   return obj1->depth>obj2->depth;
  }
 };
 
  //矩形xy与obj是否碰撞
 extern int collision_rectangle(int x1,int y1,int x2,int y2,object*obj,int prec,int notme);
}


#endif


