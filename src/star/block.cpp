#include "room.h"

namespace gml
{
 //被标记个数
 int star_block::select_num = 0;
 int star_block::select_id = 0;
 
 //star_game_ctrl构造方法
 star_game_ctrl::star_game_ctrl(int x,int y):object(x,y)
 {
  
 }
 
 //触屏事件方法
 int star_game_ctrl::event_key(int type,int p1)
 {
  if(type==KY_UP)
  	if(p1==_BACK)
  {
   room_goto(new room_star_menu());
  }
  return TRUE;
 }
 
 //星星构造方法
 star_block::star_block(int x,int y,sprite&spr):object(x,y,spr)
 {
  select = FALSE;
  image_speed = 0;
  image_index = 
  rand()%(sprite_index->number-1)+1;
 }

 //析构函数
 star_block::~star_block()
 {
  for(int i = 0;i<10;i++)
  	star_star*star = new
  star_star(x,y,*spr_star_particle);
  printf("~star_block(%d)\n",this);
 }
 
 //步方法
 void star_block::event_step()
 {
  object*obj = 
  place_meeting(x,y+16);
  if(!obj&&y<SCRH-64)
   	y = y+8;
  else
  	 y = SCRH%64+y/64*64;
 }
 
 //星星绘制方法
 void star_block::event_draw()
 {
  //绘制自己
  draw_sprite(sprite_index,
   image_index,x,y);
  //绘制选中
  if(select)
  	draw_sprite(sprite_index,
   0,x,y);
 }
 
 //触屏事件方法
 int star_block::event_mouse(int type,int x,int y)
 {
  star_block*star;
  //判断碰撞
  int w = this->sprite_index->width;
  int h = this->sprite_index->height;
  int touch = isPointCollRect(x,y,
   this->x,this->y,w,h);
  //点击
  if(touch)
  {
   //已选中
   if(select)
   {
    //消除
    if(type==MS_UP)
    	if(select_num>=2)
    {
     for(object*obj:
       object::all_object)
     {
      star = 
      dynamic_cast<star_block*>(obj);
      if(star)
      	if(star->select)
      	delete star;
     }
     select_num = 0;
     select_id = 0;
    }
   }
   //未选中
   else if(type!=MS_UP)
   {
    //清空标记
    for(object*obj:
      object::all_object)
    {
     star = 
     dynamic_cast<star_block*>(obj);
     if(star)
     	star->select = FALSE;
    }
    select_num = 0;
    select_id = 0;
    //扩散选中
    to_select();
   }
   return FALSE;
  }
  return TRUE;
 }
 
 //选中标记
 void star_block::to_select()
 {
  //已标记
  if(select)
  	return;
  
  //新增选中
  if(select_id==0
    ||select_id==image_index)
  {
   select = TRUE;
   select_id = image_index;
   select_num++;
   
   //四周扩散
   object*obj;
   star_block*star;
   obj = 
   position_meeting(x-64,y,this);
   star = 
   dynamic_cast<star_block*>(obj);
   if(star)
   	star->to_select();
   
   obj = 
   position_meeting(x+64,y,this);
   star = 
   dynamic_cast<star_block*>(obj);
   if(star)
   	star->to_select();
   
   obj = 
   position_meeting(x,y-64,this);
   star = 
   dynamic_cast<star_block*>(obj);
   if(star)
   	star->to_select();
   
   obj = 
   position_meeting(x,y+64,this);
   star = 
   dynamic_cast<star_block*>(obj);
   if(star)
   	star->to_select();
  }
 }
 
 //星星特效
 star_star::star_star(int x,int y,sprite&spr):object(x,y,spr)
 {
  //随机运动
  speed = rand()%10+2;
  direction = rand()%360;
  image_blend = 
  0xff6420|rand()%0xffffff;
  
  depth = -10;
  //自杀定时
  alarm[0] = 50;
  //透明度变化
  alarm[1] = 1;
 }
 
 //特效定时器
 void star_star::event_alarm(int index)
 {
  switch(index)
  {
   case 1:
   image_alpha-=4;
   alarm[1] = 1;
   break;
   
   case 0:
   delete this;
   break;
  }
 }
}



