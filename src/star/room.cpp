#include "room.h"

namespace gml
{
 //星星主页创建
 room_star_menu::
 room_star_menu():room()
 {
  background = 0xffffffff;
  setscrsize(640,640*SCRH/SCRW);
 }
 
 //创建主页
 void room_star_menu::event_create()
 {
  obj_button*btn;
  
  //控制器
  new star_menu_ctrl(0,0);
  
  //开始按钮
  btn = new obj_button(SCRW/2-100,
   SCRH/2-40,200,80);
  btn->depth = -20;
  btn->set_text("START");
  btn->click_fun = [](){
   room_goto(new room_star_game());
  };
  
  //退出按钮
  btn = new obj_button(SCRW/2-100,
   SCRH/2-40+150,200,80);
  btn->depth = -20;
  btn->set_text("EXIT");
  btn->click_fun = [](){
   exit();
  };
 }
 
 //星星房间创建
 room_star_game::
 room_star_game():room()
 {
  background = 0xff000000;
  setscrsize(640,640*SCRH/SCRW);
 }
 
 //星星房间创建
 void room_star_game::event_create()
 {
  new star_game_ctrl(0,0);
  
  for(int i = 0;i<10;i++)
  	for(int j = 0;j<10;j++)
  {
   star_block*star = 
   new star_block(i*64+32,j*64+32,
    *spr_star_block);
   
   //固体 
   star->solid = TRUE;
  }
 }
 
 //star_menu控制器
 star_menu_ctrl::
 star_menu_ctrl(int x,int y):
 object(x,y)
 {
  alarm[0] = 40;
 }
 
 //特效定时器
 void star_menu_ctrl::event_alarm(int index)
 {
  int x,y;
  if(index==0)
  {
   star_block(rand()%SCRW,
   rand()%SCRH,*spr_star_block);
   alarm[0] = 40;
  }
 }
}