#ifndef STAR_ROOM_H
#define STAR_ROOM_H

#include "../base.h"
#include "../android.h"
#include "../ex_math.h"
#include "../graphics.h"

#include "../gms3/gml.h"
#include "../gms3/room.h"
#include "../gms3/object.h"
#include "../gms3/sprite.h"

#include "../moudle/moudle.h"

namespace gml
{
 //声明精灵
 extern sprite *spr_star_block;
 extern sprite *spr_star_particle;
 
 //消息传递
 extern int event(int type,int p1,int p2);
 
 //星星主页
 class room_star_menu:public room
 {
  public:
  //构造函数
  room_star_menu();
  void event_create();
 };
 
 //星星房间
 class room_star_game:public room
 {
  public:
  //构造函数
  room_star_game();
  void event_create();
 };
 
 //star_menu控制器
 class star_menu_ctrl:public object
 {
  public:
  //构造方法
  star_menu_ctrl(int x,int y);
  //定时器
  void event_alarm(int index);
 };
 
 //star_game控制器
 class star_game_ctrl:public object
 {
  public:
  //构造方法
  star_game_ctrl(int x,int y);
  int event_key(int type,int p1);
 };
 
 //星星类
 class star_block:public object
 {
  public:
  //被标记个数
  static int select_num;
  static int select_id;
  
  //是否被标记
  int select;
  
  //扩散标记
  void to_select();

  //构造方法
  star_block(int x,int y,sprite&spr);
  ~star_block();
  
  //步方法
  void event_step();
  //绘制方法
  void event_draw();
  //触屏方法
  int event_mouse(int type,int x,int y);
 };
 
 //星星特效
 class star_star:public object
 {
  public:
  star_star(int x,int y,sprite &spr);
  
  //定时器
  void event_alarm(int index);
 };
}


#endif
