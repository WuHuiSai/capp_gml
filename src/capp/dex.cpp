#include "dex.h"

//创建插件
//参数：插件文件名，插件类名
_DEX*dex_create(char*filename,char*classname)
{
 _DEX*dex = 
 (_DEX*)malloc(sizeof(_DEX));
 memset(dex,0,sizeof(_DEX));
 
 dex->loader = loadDex(filename);
 if(dex->loader!=0)
 {
  printf("%d \n",dex->loader);
  dex->cls = 
  loadClass(dex->loader,classname);
  
  if(dex->cls!=0)
  {
   dex->obj = runClass(dex->cls);
  }
  else
  {
   printf("找不到class \n");
  }
 }
 else
 {
  printf("loader null \n");
 }
 
 return dex;
}


//向插件传递数据
char*dex_put(_DEX*dex,char*text)
{
 return dex_putString(dex->obj,text);
}


//释放插件
int dex_free(_DEX*dex)
{
 free(dex);
 return 0;
}


