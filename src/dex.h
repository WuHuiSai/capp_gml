#ifndef _DEX_H_
#define _DEX_H_

#include "base.h"
#include "exb.h"
#include "android.h"

typedef struct
{
 int loader;
 int cls;
 int obj;
}_DEX;

//创建插件
//参数：插件文件名，插件类名
extern _DEX*dex_create(char*filename,char*classname);


//向插件传递数据
extern char*dex_put(_DEX*dex,char*text);


//释放插件
extern int dex_free(_DEX*dex);


#endif








