#include "moudle.h"

namespace gml
{
 //创建按钮
 obj_button::obj_button(int x,int y,int w,int h):object(x,y)
 {
  this->w = w;
  this->h = h;
  
  state = 0;
  
  bkg[0] = 0xff75c0f0;
  bkg[1] = 0xff65aef0;
  
  text = "";
  click_fun = [](){
   toast((char*)"click",0);
  };
 }
 
 //触屏反馈
 int obj_button::event_mouse(int type,int x,int y)
 {
  if(type==MS_DOWN)
  	if(isPointCollRect(x,y,this->x,
     this->y,this->w,this->h))
  	state = 1;
  else
  	state = 0;
  
  if(type==MS_UP)
  {
   if(state==1)
   	if(isPointCollRect(x,y,this->x,
      this->y,this->w,this->h))
   	click_fun();
   
   state = 0;
  }
  return TRUE;
 }
 
 //绘制按钮
 void obj_button::event_draw()
 {
  int tw,th;
  char*t = (char*)text.c_str();
  textwh(t,FALSE,0,&tw,&th);
  drawRect(x,y,w,h,bkg[state%2]);
  dtext((char*)text.c_str(),
   x+(w-tw)/2,y+(h-th)/2,
   0xff,0xff,0xff,FALSE,0);
 }
}