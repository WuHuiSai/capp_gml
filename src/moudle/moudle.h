#ifndef MOUDLE_H
#define MOUDLE_H

#include "../base.h"
#include "../android.h"
#include "../ex_math.h"
#include "../graphics.h"

#include "../gms3/gml.h"
#include "../gms3/room.h"
#include "../gms3/object.h"
#include "../gms3/sprite.h"

#include <string>

namespace gml
{
 using std::string;
 class obj_button:public object
 {
  public:
  //按钮状态
  int state;
  //宽高
  int w;
  int h;
  //背景色
  int bkg[2];
  //文本
  string text;
  
  //触发事件
  void (*click_fun)();
  
  //创建按钮
  obj_button(int x,int y,int w,int h);
  
  //设置文字
  virtual void set_text(string text)
  {
   this->text = text;
  }
  
  //触屏反馈
  virtual int event_mouse(int type,int x,int y);
  //绘制按钮
  virtual void event_draw(); 
 };
}

#endif
