
#ifndef DEF_H
#define DEF_H

//加载系统头文件base.h
#include "base.h"
#include "exb.h"
#include "ex_math.h"
#include "graphics.h"
#include "android.h"

#include "pad.h"

#include "gms3/gml.h"
#include "gms3/room.h"
#include "gms3/object.h"
#include "gms3/sprite.h"

#include "moudle/moudle.h"

namespace gml
{
 //消灭星星精灵
 extern sprite *spr_star_block;
 extern sprite *spr_star_particle;
 
 //载入资源
 extern void init_res();
 
}

#endif






