#include "def.h"

namespace gml
{
 //消灭星星精灵
 sprite *spr_star_block;
 sprite *spr_star_particle;
 
 //载入资源
 void init_res()
 {
  spr_star_block = new
  sprite("sprite/star/block",32,32);
  spr_star_particle = new
  sprite("sprite/star/particle");
 }
}


