
#include "def.h"
#include <vector>

#include "star/room.h"

#ifdef __cplusplus
extern "C"{
#endif
using namespace gml;

int main()
{
 //初始化键盘
 /*
 setContextView((char*)
  "xml/main.xml");
 initPad((char*)"addView 100");*/
 
 //初始化资源
 init_res();

 //跳转房间
 room_goto(new room_star_menu());
 return 0;
}

#ifdef __cplusplus
}
#endif

