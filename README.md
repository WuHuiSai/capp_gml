## GMS3
仿制`GameMakerStudio`游戏引擎

### 待办事项
- 增加`class`过滤碰撞

### 更新日志

#### `06.20`
- 增加`solid`属性
- `place_meeting`判断`solid`属性
- 更改例子为消灭星星

#### `06.15`
- `object`增加`debug`静态属性
- 可创建空`sprite`的`object`
- 增加`obj_button`模板

#### `06.14`
- 深度改为越小越靠前
- 修复碰撞盒问题

#### `06.14`
- 跳转房间清空非`keep`的`object`
- 增加`pause`、`resume`接口
- `screen_width`、`screen_height`
- 增加`main`入口函数
- 修复触屏事件与视野冲突
- `object`增加`visiable`属性

#### `06.13`
- 房间`视野`结构完成
- `room_goto`时才调用`room`的`create`事件
- 更新碰撞方法，使用bbox

#### `06.13`
- 框架`基本结构`搭建完毕

## 使用方法
初始化`sprite`，定义生成`object`  
定义生成`room`，`room_goto`你的`room`即可

### 载入`sprite`
载入的`sprite`在应用退出时会自动释放
- 第二个和第三个参数是sprite中心点
```
//载入assets内sprite/spr_bomb
sprite *spr_bomb = 
new sprite("sprite/spr_bomb",0,0);
```

### 定义`object`
继承`object`，构造方法调用`object`的构造方法即可

```
//继承object
class obj_girl:public object
{
  public:
  //构造方法
  rmLogo(int x,int y,sprite&spr):
  object(x,y,spr)
  {
   
  }  
};
//若要实现自定义绘制，响应触屏等操作，实现对应的方法即可
```

### 定义`room`
定义`room`和定义`object`类似，继承`room`即可  
- 当使用`room_goto`跳转房间时会调用对应房间的`event_create`方法

```
//房间
class rmLogo:public room
{
  public:
  rmLogo():room()
  {
   
  }
  
  //创建事件
  void event_create()
  {
   for(int i = 0;i<10;i++)
   for(int j = 0;j<10;j++)
   {
    object*obj = 
    new object(i*40+50,j*40+50,
    *spr_item);
    obj->image_speed = 0;
    obj->image_index = rand()%
    obj->sprite_index->number-2;
   }
   printf("logo%d create()\n",this);
  }
};
```

